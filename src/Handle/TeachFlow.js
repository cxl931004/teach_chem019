var /**
	 * 仿真流程管理
	 */
TeachFlow = cc.Class.extend({
	step:0,
	flow:null,
	main:null,
	over_flag: false,
	curSprite:null,
	ctor: function(){
		this.init();
	},
	setMain:function(main){
		this.main=main;
	},
	init: function(){
		for(var i in teach_flow){
			teach_flow[i].finish = false;
			teach_flow[i].cur = false;
			if(teach_flow[i].action == null){
				teach_flow[i].action = ACTION_NONE;
			}
		}
	},
	start:/**
			 * 开始流程
			 */
	function(){
		this.over_flag = false;
		this.step = 0;
		
		/* 新标准，开始的时候，执行下一步 */
		this.next();
	},
	over:/**
			 * 流程结束，计算分数，跳转到结束场景，
			 */
	function(){
		this.over_flag = true;
		this.flow = over;
		gg.lastStep = this.step;
		this.main.over();
	},
	checkTag:/**
				 * 检查是否当前步骤
				 * 
				 * @deprecated 使用新Angel类，不再判断是否当前步骤
				 * @param tag
				 * @returns {Boolean}
				 */
	function(tag){
		var cur_flow = teach_flow[this.step - 1];
		if(cur_flow.tag == tag){
			return true;
		} else {
			return false;
		}
	},
	prev:/**
			 * 回退一定步数
			 * 
			 * @deprecated 需结合具体实现，，暂时不再启动
			 * @param count
			 *            步数
			 */
	function(count){
		if(this.curSprite!=null){
			this.curSprite = null;
		}
		if(this.flow!=null){
			this.flow.cur = false;
		}
		this.step = this.step - count;
		this.flow = teach_flow[this.step - 1];
		this.refresh();
		gg.score -= 11;
	},
	next:/**
			 * 执行下一步操作， 定位当前任务
			 */
	function(){
		if(this.over_flag){
			return;
		}
		if(this.curSprite!=null){
			this.curSprite.setEnable(false)
			this.curSprite = null;
		}
		if(this.flow!=null){
			this.flow.cur = false;
			// 标记任务已完成
			this.flow.finish = true;
		}
		this.flow = teach_flow[this.step++];
		if(this.flow.finish){
			// 如果任务已完成，跳过当前步骤
			this.next();
		}
		this.refresh();
	},
	refresh:/**
			 * 刷新当前任务状态，设置闪现，点击等状态
			 */
	function(){
		// 刷新提示
		this.flow.cur = true;
		if(this.flow.tip != null){
			ll.tip.tip.doTip(this.flow.tip);
		}
		if(this.flow.flash != null){
			ll.tip.flash.doFlash(this.flow.flash);
		}
		if(this.step > teach_flow.length - 1){
			this.over();
		}
		this.initCurSprite();
		if(this.curSprite!=null){
			this.location();
			this.curSprite.setEnable(true);
		}
	},
	location:/**
	 * 定位箭头
	 */
		function(){
		var tag = gg.flow.flow.tag;
		if(tag instanceof Array){
			tag=tag[1];
			if(TAG_LIB_MIN < tag[1]){
				if(ll.run.lib.isOpen()){
					ll.tip.arr.pos(this.curSprite);
				}else{
					//ll.tip.arr.setPosition(gg.width-45,455);

					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));
				}
			}else{
				ll.tip.arr.pos(this.curSprite);
			}
		}
		else {
			ll.tip.arr.pos(this.curSprite);
		}		
	},
	getStep:/**
			 * 获取当前步数
			 * 
			 * @returns {Number}
			 */
	function(){
		return this.step;
	},
	initCurSprite:/**
					 * 遍历获取当前任务的操作对象
					 */
	function(){
		var tag = this.flow.tag;
		if(tag == null || tag == undefined){
			return;
		}
		var root = ll.run;
		var sprite = null;
		if(tag == TAG_BUTTON_LIB){
			sprite = ll.tool.getChildByTag(tag);
		} else if(tag instanceof Array){
			// 数组
			for (var i in tag) {
				root = root.getChildByTag(tag[i]);
			}
			sprite = root;
		} else {
			// 单个tag
			var sprite = root.getChildByTag(tag);
		}
		if(sprite != null){
			this.curSprite = sprite;
			return;
		}
	}
});
// 任务流
teach_flow = [
// 取5g粗食盐
	{tip:"1.打开物品库",tag:TAG_BUTTON_LIB},
	{tip:"取出称量纸,将称量纸放在天平两侧",tag:[TAG_LIB,TAG_LIB_PAPER]},
	{tip:"查看天平是否平衡",tag:[TAG_BALANCE_ALL,TAG_BALANCE,TAG_INSTRUCTIONS]},
	{tip:"调节天平两侧的螺丝将天平调平",tag:[TAG_BALANCE_ALL,TAG_BALANCE,TAG_NUT]},
	{tip:"将游码拨到0.8g位置,此时天平向右边倾斜",tag:[TAG_BALANCE_ALL,TAG_BALANCE,TAG_RIDER]},
	{tip:"取出装有高锰酸钾粉末的棕色瓶",tag:[TAG_LIB,TAG_LIB_BOTTLE]},
	{tip:"打开广口棕色瓶盖子",tag:[TAG_BOTTLE_ALL,TAG_LID]},
	{tip:"取出药匙",tag:[TAG_LIB,TAG_LIB_SPOON]} ,
	{tip:"用药匙将棕色瓶中的高锰酸钾粉末拨到称量纸上,待天平重新平衡为止",tag:[TAG_SPOON_ALL,TAG_SPOON]},
	{tip:"将广口棕色瓶盖子盖上",tag:[TAG_BOTTLE_ALL,TAG_LID],action:ACTION_DO1},
	{tip:"取出锥心瓶",tag:[TAG_LIB,TAG_LIB_FLASK]},
	{tip:"将称量纸上的高锰酸钾粉末倒入锥形瓶中",tag:[TAG_PAPER_ALL,TAG_PAPER]},
	{tip:"取出量筒",tag:[TAG_LIB,TAG_LIB_CYLINDER]},
	{tip:"取出蒸馏水瓶",tag:[TAG_LIB,TAG_LIB_KETTLE]},
	{tip:"将蒸馏水倒入量筒中,待水位线达到250ml刻度线为止",tag:[TAG_KETTLE_ALL,TAG_KETTLE]},
	{tip:"将量筒中的蒸馏水倒入锥形瓶中",tag:[TAG_CYLINDER_ALL,TAG_CYLINDER]},
	{tip:"取出玻璃棒,搅拌锥形瓶中的溶液使高锰酸钾粉末溶解",tag:[TAG_LIB,TAG_LIB_ROD]},
	{tip:"取出铁架台",tag:[TAG_LIB,TAG_LIB_SHELF]},
	{tip:"取出石棉网,将其盖在铁架台上",tag:[TAG_LIB,TAG_LIB_ASBESTOS]},
	{tip:"将锥形瓶放在铁架台上",tag:[TAG_FLASK_ALL,TAG_FLASK],action:ACTION_DO1},
	{tip:"取出表面皿将其盖在锥形瓶上",tag:[TAG_LIB,TAG_LIB_DISH]},
	{tip:"取出酒精灯",tag:[TAG_LIB,TAG_LIB_LAMP]},
	{tip:"将酒精灯上的酒精灯帽取下",tag:[TAG_LAMP_ALL,TAG_LAMP1],action:ACTION_DO1},
	{tip:"取出火柴盒,将火柴点燃并点燃酒精灯",tag:[TAG_LIB,TAG_LIB_MATCH]},
	{tip:"将酒精灯移入铁架台下面,煮沸高锰酸钾溶液15分钟",tag:[TAG_LAMP_ALL,TAG_LAMP],action:ACTION_DO1},
	{tip:"移去酒精灯,将酒精灯帽盖在酒精灯上,等待高锰酸钾溶液冷却",tag:[TAG_LAMP_ALL,TAG_LAMP],action:ACTION_DO2},
	{tip:"将酒精灯帽盖在酒精灯上",tag:[TAG_LAMP_ALL,TAG_LAMP1],action:ACTION_DO2},
	{tip:"取出棕色试剂瓶",tag:[TAG_LIB,TAG_LIB_BOTTLE1]},
	{tip:"打开棕色试剂瓶盖子",tag:[TAG_BOTTLE1_ALL,TAG_BOTTLE1_LID],action:ACTION_DO1},
	{tip:"取下玻璃皿",tag:[TAG_DISH_ALL,TAG_DISH]},
	{tip:"将锥形瓶中溶解后的溶液移入试剂瓶中",tag:[TAG_FLASK_ALL,TAG_FLASK],action:ACTION_DO2},
	{tip:"盖上棕色试剂瓶盖子",tag:[TAG_BOTTLE1_ALL,TAG_BOTTLE1_LID],action:ACTION_DO2},
	{tip:"恭喜过关",over:true}
];
over = {tip:"恭喜过关"};



