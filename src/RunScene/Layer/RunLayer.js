var RunLayer = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.balance=new Balance(this);
		this.balance.setPosition(gg.width*0.2,gg.height*0.4);
		this.addChild(this.balance, 9);
		this.scheduleOnce(function(){
		},1);
	},
	loadTest:function(){
 this.lib.moveLib(TAG_LIB_SALT,400);
	},
	test2:function(){
		var move = cc.moveTo(0.7,cc.p(570,320));
		var rotate = cc.rotateTo(0.8, 90);
		var func = cc.callFunc(function(){
			ss.pour2();
			ss.hide(3, true);
		},this);
		var seq = cc.sequence(move, rotate, func);
		ss.runAction(seq);
	},
	loadPaper:function(pos){
		var paper=new Paper(this);
		this.addChild(paper,10);
		this.loadInLib(paper, pos,cc.p(gg.width*0.14, gg.height*0.45),1);
		
		var paper1=new Button(this,9,TAG_PAPER1,"#paper.png",this.callback,this);
		this.loadInLib(paper1, pos,cc.p(gg.width*0.26, gg.height*0.45),0);
	},
	loadBottle:function(pos){
		var bottle=new Bottle(this);
		this.addChild(bottle, 11);
		this.loadInLib(bottle, pos,cc.p(gg.width*0.4, gg.height*0.4),1);
	},
	loadSpoon:function(pos){
		var spoon=new Spoon(this);
		this.addChild(spoon,10);
		this.loadInLib(spoon, pos,cc.p(gg.width*0.55,gg.height*0.4),1);
	},
	loadFlask:function(pos){
		var flask=new Flask(this);
		this.addChild(flask,11);
		this.loadInLib(flask, pos,cc.p(gg.width*0.4, gg.height*0.4),1);
	},
	loadKettle:function(pos){
		var kettle=new Kettle(this);
		this.addChild(kettle,11);
		this.loadInLib(kettle, pos,cc.p(gg.width*0.7, gg.height*0.4),1);
	},
	loadCylinder:function(pos){
		var cylinder=new Cylinder(this);
		this.addChild(cylinder,11);
		this.loadInLib(cylinder, pos,cc.p(gg.width*0.55, gg.height*0.4),1);
	},
	loadRod:function(pos){
		var rod=new Rod(this);
		this.addChild(rod,10);
		this.loadInLib(rod, pos,cc.p(gg.width*0.4, gg.height*0.51),1);
	},
	loadShelf:function(pos){
		var shelf=new Shelf(this);
		this.addChild(shelf, 11);
		this.loadInLib(shelf, pos,cc.p(gg.width*0.25, gg.height*0.3),1);
	},
	loadAsbestos:function(pos){
		var asbestos=new Asbestos(this);
		this.addChild(asbestos,11);
		this.loadInLib(asbestos, pos,cc.p(gg.width*0.245, gg.height*0.368),1);
	},
	loadDish:function(pos){
		var dish=new Dish(this);
		this.addChild(dish,12);
		this.loadInLib(dish, pos,cc.p(gg.width*0.25, gg.height*0.52),1);
	},
	loadLamp:function(pos){
		var lamp=new Lamp(this);
		this.addChild(lamp, 10);
		this.loadInLib(lamp, pos,cc.p(gg.width*0.45, gg.height*0.3),1)
	},
	loadMatch:function(pos){
		var match=new Match(this);
		this.addChild(match, 11);
		this.loadInLib(match, pos,cc.p(gg.width*0.65, gg.height*0.3),0);
	},
	loadBottle1:function(pos){
		var bottle1=new Bottle1(this);
		this.addChild(bottle1, 11);
		this.loadInLib(bottle1, pos,cc.p(gg.width*0.55, gg.height*0.4),1);
	},
	loadK2CrO4:function(pos){
		var k2CrO4=new K2CrO4(this);
		this.addChild(k2CrO4, 11);
		this.loadInLib(k2CrO4, pos,cc.p(gg.width*0.65, gg.height*0.4))
	},
	loadInLib:function(obj, pos, tarPos,state,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		if(state==1){
			var seq = cc.sequence(ber, this.callNext);
		}else{
			var seq=cc.sequence(ber);
		}
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	}
});