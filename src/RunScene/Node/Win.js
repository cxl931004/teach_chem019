Win = cc.Node.extend({
	main:null,
	ctor:function(p, name){
		this._super();
		p.addChild(this, 30);	
		this.init(name);
	},
	init:function(name){
		this.setCascadeOpacityEnabled(true);
		this.setPosition(100, 100);
		this.setScale(0);
		this.main = new cc.Sprite(name);
		this.addChild(this.main,1,TAG_MAIN);

		var mp = this.main.getAnchorPoint();
	},
	show:function(next){
		var move = cc.moveTo(0.5, gg.width * 0.2, gg.height * 0.5);
		var scale = cc.scaleTo(0.5, 1);
		var spawn = cc.spawn(move, scale);
		var seq = cc.sequence(spawn, cc.callFunc(function(){
			if(next){
				gg.flow.next();
			}
		},this));
		this.runAction(seq);
	}
});