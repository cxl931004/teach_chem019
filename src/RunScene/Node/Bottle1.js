//棕色瓶
Bottle1 = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.initAction();
		this.setTag(TAG_BOTTLE1_ALL);
	},
	init:function(){
		this.body = new Button(this, 10, TAG_BOTTLE1, "#bottle/bottle.png", this.callback,this);
		
		var lid=new Button(this,9,TAG_BOTTLE1_LID,"#bottle/lid.png",this.callback,this);
		lid.setPosition(cc.p(-this.body.width*0.05, this.body.height*0.38));
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<6;i++){
			var str="action1/bottle"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.1)
		this.runningAction=cc.repeat(cc.animate(animation),1);
		this.runningAction.retain();
	},
	showBottle:function(){
		var bottle=this.getChildByTag(TAG_BOTTLE1);
		bottle.runAction(this.runningAction);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_BOTTLE1_LID:
			var ber=cc.bezierBy(1, [cc.p(10,25),cc.p(45	,55),cc.p(68,-130)]);
			var rotate=cc.rotateBy(1,180);
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(cc.spawn(ber,rotate),func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(cc.spawn(ber.reverse(),rotate.reverse()),func));
			}
			break;
		default:
			break;
		}
	}
});