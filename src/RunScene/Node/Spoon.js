Spoon = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_SPOON_ALL);
	},
	init:function(){
		this.body = new Button(this, 10, TAG_SPOON, "#spoon.png", this.callback,this);
	},
	back:function(){
		var bottle=ll.run.getChildByTag(TAG_BOTTLE_ALL);
		var body=bottle.getChildByTag(TAG_BOTTLE);
		var ber1=cc.bezierBy(1, [cc.p(-25,50),cc.p(-230,65),cc.p(-288, 80)]);
		var rotate=cc.rotateBy(1,-90);
		var balance=ll.run.getChildByTag(TAG_BALANCE_ALL);
		body.runAction(cc.sequence(cc.spawn(ber1.reverse(),rotate.reverse()),cc.callFunc(function() {
			balance.show4();
			body.setSpriteFrame("bottle/bottle1.png");
		},this)));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		switch (p.getTag()) {
		case TAG_SPOON:
			var bottle=ll.run.getChildByTag(TAG_BOTTLE_ALL);
			var body=bottle.getChildByTag(TAG_BOTTLE);
			var paper=ll.run.getChildByTag(TAG_PAPER_ALL);
			var body1=paper.getChildByTag(TAG_PAPER);
			var powder=body1.getChildByTag(TAG_POWDER);
			var balance=ll.run.getChildByTag(TAG_BALANCE_ALL);
			var ber=cc.bezierBy(1, [cc.p(-25, 50),cc.p(-268, 65),cc.p(-568,50)]);
			var ber1=cc.bezierBy(1, [cc.p(-25,50),cc.p(-230,65),cc.p(-288, 80)]);
			var rotate=cc.rotateBy(1,-90);
			var rotate1=cc.rotateBy(1,-180);
			var move=cc.moveBy(0.5,cc.p(20, 0));
			var scale=cc.scaleTo(1,1);
			p.runAction(cc.sequence(
			//药匙和棕色瓶的移动
			cc.spawn(cc.spawn(ber,rotate1),cc.callFunc(function() {
				body.runAction(cc.sequence(cc.spawn(ber1,rotate),cc.callFunc(function() {
					body.setSpriteFrame("bottle/bottle3.png");
				}, this)));
			},this)),
			cc.spawn(cc.sequence(move,move.reverse(),move,move.reverse()),cc.sequence(cc.callFunc(function() {
				paper.showPowder();
			}, this))),cc.callFunc(function() {
				this.back();
			},this),cc.callFunc(function() {
				p.setOpacity(0);
			},this),func
			));
			break;
		default:
			break;
		}
	}
});