//蒸馏水瓶
Kettle = Widget.extend({
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_KETTLE_ALL);
	},
	init:function(){
		this.body = new Button(this, 10, TAG_KETTLE, "#kettle.png", this.callback,this);
		
		var label=new Button(this.body,10,TAG_LABEL,"#label.png",this.callback,this);
		label.setPosition(cc.p(this.body.width*0.5,this.body.height*0.35));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_KETTLE:
			var ber=cc.bezierBy(1, [cc.p(-10, 35),cc.p(-25,55),cc.p(-85, 100)]);
			var rotate=cc.rotateBy(1,-85);
			var cylinder=ll.run.getChildByTag(TAG_CYLINDER_ALL);
			p.runAction(cc.sequence(ber,rotate,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				cylinder.showFlow();
			},this)),cc.callFunc(function() {
				cylinder.show();
			},this),rotate.reverse(),cc.callFunc(function() {
				p.setVisible(false);
			},this),func));
			break;
		default:
			break;
		}
	}
});