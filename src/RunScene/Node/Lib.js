/**
 * 药品库
 */
Lib = cc.Sprite.extend({
	libArr: [],
	openFlag:false,
	doing:false,
	rm : 5,
	ctor:function(p){
		this._super("#lib_back.png");
		p.addChild(this,30);
		this.setTag(TAG_LIB);
		this.init();
	},
	init:function(){
		this.libArr = [];
		this.openFlag = false;
		this.setPosition(gg.width + this.width * 0.5,550);
		
		var paper=new LibButton(this,10,TAG_LIB_PAPER,"#paper.png",this.callback,this);
		paper.setPosition(cc.p(60, this.height*0.5));
		
		var bottle=new LibButton(this,10,TAG_LIB_BOTTLE,"#bottle/bottle1.png",this.callback,this);
		bottle.right(paper, this.rm);
		
		var spoon=new LibButton(this,10,TAG_LIB_SPOON,"#spoon.png",this.callback,this);
		spoon.right(bottle,this.rm);
		
		var flask=new LibButton(this,10,TAG_LIB_FLASK,"#flask.png",this.callback,this);
		flask.right(spoon,this.rm);
		
		var cylinder=new LibButton(this,10,TAG_LIB_CYLINDER,"#cylinder.png",this.callback,this);
		cylinder.right(flask,this.rm);
		
		var kettle=new LibButton(this,10,TAG_LIB_KETTLE,"#kettle.png",this.callback,this);
		kettle.right(cylinder,this.rm);
		
		var rod=new LibButton(this,10,TAG_LIB_ROD,"#rod.png",this.callback,this);
		rod.right(kettle,this.rm);
		
		var shelf=new LibButton(this,10,TAG_LIB_SHELF,"#shelf.png",this.callback,this);
		shelf.right(rod,this.rm);
		
		var asbestosNet=new LibButton(this,10,TAG_LIB_ASBESTOS,"#asbestosNet.png",this.callback,this);
		asbestosNet.right(shelf,this.rm);
		
		var dish=new LibButton(this,10,TAG_LIB_DISH,"#dish.png",this.callback,this);
		dish.right(asbestosNet, this.rm);
		
		var lamp=new LibButton(this,10,TAG_LIB_LAMP,"#lamp/lp1.png",this.callback,this);
		lamp.right(dish,this.rm);
		
		var match=new LibButton(this,10,TAG_LIB_MATCH,"#lamp/match_box.png",this.callback,this);
		match.right(lamp,this.rm);
		
		var bottle1=new LibButton(this,10,TAG_LIB_BOTTLE1,"#bottle/bottle.png",this.callback,this);
		bottle1.right(match, this.rm);
		
	},
	moveLib:function(tag,width){
		width = 75;
		var begin = false;
		for(var i in this.libArr){
			var libTag = this.libArr[i];
			if(tag == libTag){
				begin = true;
			}
			if(begin){
				var lib = this.getChildByTag(libTag);
				if(lib != null){
					lib.runAction(cc.moveBy(0.5,cc.p(-width, 0)));
				}
			}
		}
	},
	callback:function(p){
		var pos = this.getPosition(); 
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_LIB_PAPER:
			ll.run.loadPaper(pos);
			break;
		case TAG_LIB_BOTTLE:
			ll.run.loadBottle(pos);
			break;
		case TAG_LIB_SPOON:
			ll.run.loadSpoon(pos);
			break;
		case TAG_LIB_FLASK:
			ll.run.loadFlask(pos);
			break;
		case TAG_LIB_KETTLE:
			ll.run.loadKettle(pos);
			break;
		case TAG_LIB_CYLINDER:
			ll.run.loadCylinder(pos);
			break;
		case TAG_LIB_ROD:
			ll.run.loadRod(pos);
			break;
		case TAG_LIB_SHELF:
			ll.run.loadShelf(pos);
			break;
		case TAG_LIB_ASBESTOS:
			ll.run.loadAsbestos(pos);
			break;
		case TAG_LIB_DISH:
			ll.run.loadDish(pos);
			break;
		case TAG_LIB_LAMP:
			ll.run.loadLamp(pos);
			break;
		case TAG_LIB_MATCH:
			ll.run.loadMatch(pos);
			break;
		case TAG_LIB_BOTTLE1:
			ll.run.loadBottle1(pos);
			break;
		default:
			break;
		}
		if(action == ACTION_NONE){
			this.moveLib(p.getTag(), p.width * p.getScale());
			p.removeFromParent(true);
		}
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		if(this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(-this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			 if(tag instanceof Array){
				 if(TAG_LIB_MIN < tag[1]){
					 // 显示箭头
					 gg.flow.location();
				 }
			 }
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	},
	close:function(){
		if(!this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					ll.tip.arr.out();
				}
			}
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	}
});
TAG_LIB_MIN = 30000;
TAG_LIB_PAPER = 30001;
TAG_LIB_BOTTLE = 30002;
TAG_LIB_SPOON = 30003;
TAG_LIB_FLASK = 30004;
TAG_LIB_KETTLE = 30005;
TAG_LIB_CYLINDER = 30006;
TAG_LIB_ROD = 30007;
TAG_LIB_SHELF = 30008;
TAG_LIB_ASBESTOS = 30009;
TAG_LIB_DISH = 30010;
TAG_LIB_LAMP = 30011;
TAG_LIB_MATCH = 30012;
TAG_LIB_BOTTLE1=30013;
TAG_LIB_K2CR04=30014;
libRelArr = [
             {tag:TAG_LIB_MIN, name:""},
             {tag:TAG_LIB_PAPER, name:"称量纸"},
             {tag:TAG_LIB_BOTTLE,name:"广口棕色瓶"},
             {tag:TAG_LIB_SPOON,name:"药匙"},
             {tag:TAG_LIB_FLASK,name:"锥形瓶"},
             {tag:TAG_LIB_KETTLE,name:"蒸馏水瓶"},
             {tag:TAG_LIB_CYLINDER,name:"量筒"},
             {tag:TAG_LIB_ROD,name:"玻璃棒"},
             {tag:TAG_LIB_SHELF,name:"铁架台"},
             {tag:TAG_LIB_ASBESTOS,name:"石棉网"},
             {tag:TAG_LIB_DISH,name:"玻璃皿"},
             {tag:TAG_LIB_LAMP,name:"酒精灯"},
             {tag:TAG_LIB_MATCH,name:"火柴盒"},
             {tag:TAG_LIB_BOTTLE1,name:"细口棕色瓶"}
             ];
