Flask = Widget.extend({
	runningAction:null,
	runningAction1:null,
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_FLASK_ALL);
		this.initAction();
	},
	init:function(){
		//锥形瓶
		this.body = new Button(this, 10, TAG_FLASK, "#flask.png", this.callback,this);
		
		var powder1=new Button(this.body,10,TAG_POWDER1,"#powder.png",this.callback,this);
		powder1.setPosition(cc.p(this.body.width*0.5, this.body.height*0.08));
		powder1.setOpacity(0);
		powder1.setScale(1,0.3);
		
		var flow=new Button(this.body,10,TAG_FLOW,"#flow.png",this.callback	,this);
		flow.setPosition(cc.p(this.body.width*0.5, this.body.height*0.6));
		flow.setOpacity(0);
		
		var film=new Button(this.body,10,TAG_FILM1,"#beaker_line.png",this.callback,this);
		film.setPosition(cc.p(this.body.width*0.5, this.body.height*0.05));
		film.setOpacity(0);
		
		var flask=new Button(this.body,10,TAG_FLASK1,"#flask1.png",this.callback,this);
		flask.setPosition(cc.p(this.body.width*0.5,this.body.height*0.5));
		flask.setOpacity(0);
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<5;i++){
			var str="action/flask"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.1)
		this.runningAction=cc.repeat(cc.animate(animation),1);
		this.runningAction.retain();
	},
	showFlask:function(){
		var flask=this.body.getChildByTag(TAG_FLASK1);
		var powder1=this.body.getChildByTag(TAG_POWDER1);
		var film=this.body.getChildByTag(TAG_FILM1);
		var fadein=cc.fadeIn(4);
		var fadeout=cc.fadeOut(4);
		flask.runAction(cc.spawn(fadein,cc.callFunc(function() {
			powder1.runAction(fadeout);
			film.setVisible(false);
		},this)));
	},
	showPowder1:function(){
		var fadein=cc.fadeIn(1);
		var scale=cc.scaleBy(1,1.6, 2);
		var powder1=this.body.getChildByTag(TAG_POWDER1);
		powder1.runAction(cc.sequence(fadein,scale,cc.delayTime(1)));
	},
	showFlow:function(){
		var fadein=cc.fadeIn(1);
		var fadeout=cc.fadeOut(1);
		var flow=this.body.getChildByTag(TAG_FLOW);
		flow.runAction(cc.sequence(fadein,cc.delayTime(0.5),fadeout));
	},
	showFilm:function(){
		var fadein=cc.fadeIn(0.5);
		var scale=cc.scaleBy(1,0.5);
		var move=cc.moveBy(1,cc.p(0,this.body.height*0.65));
		var film=this.body.getChildByTag(TAG_FILM1);
		film.runAction(cc.sequence(fadein,cc.spawn(move,scale)));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		var action=gg.flow.flow.action;
		var ber=cc.bezierBy(1, [cc.p(-10,15),cc.p(-25, 30),cc.p(-195,30)]);
		var ber1=cc.bezierBy(1, [cc.p(25, 15),cc.p(145, 55),cc.p(320,50)]);
		var rotate=cc.rotateBy(1,85);
		var bottle1=ll.run.getChildByTag(TAG_BOTTLE1_ALL);
		var flask=this.body.getChildByTag(TAG_FLASK1);
		switch (p.getTag()) {
		case TAG_FLASK:
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(cc.callFunc(function() {
					this.setLocalZOrder(12);
				},this),ber,func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(ber1,cc.spawn(this.runningAction,cc.callFunc(function() {
					flask.setVisible(false);
				},this),cc.sequence(cc.delayTime(0.3),cc.callFunc(function() {
					bottle1.showBottle();
				}, this))),this.runningAction.reverse(),cc.callFunc(function() {
					p.setVisible(false);
				},this),func));
			}
			break;
		default:
			break;
		}
	}
});