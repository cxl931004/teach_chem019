DiskPoint = cc.Sprite.extend({
	ctor:function(){
		this._super("#disk.png");
		this.init();
	},
	init:function(){
		this.setOpacity(0);
		this.setTag(TAG_DISK_POINT);
		
		// 影响子节点透明度
		this.setCascadeOpacityEnabled(true)
		
		var point = new cc.Sprite("#point.png");
		point.setPosition(48,0);
		point.setAnchorPoint(0.5,0);
		point.setTag(TAG_KILL);
		this.addChild(point);
		
		this.func = cc.callFunc(this.actionDone, this);
	},
	show:function(){
		var fade = cc.fadeTo(0.5, 255);
		var seq = cc.sequence(fade,this.func);
		this.runAction(seq);
	},
	actionDone:function(p){
		if(p.getTag() == TAG_DISK_POINT){
			var point = this.getChildByTag(TAG_KILL);
			var rotate1 = cc.rotateTo(0.25, 10);
			var rotate2 = cc.rotateTo(0.5, -10);
			var rotate3 = cc.rotateTo(0.25, 0);
			var delay = cc.delayTime(0.5);
			var seq = cc.sequence(rotate1, rotate2, rotate3, delay, this.func);
			point.runAction(seq);	
		} else if(p.getTag() == TAG_KILL){
			this.runAction(cc.fadeTo(0.5, 0));
			gg.flow.next();
		}
	}
}); 