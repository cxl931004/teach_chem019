Balance = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_BALANCE_ALL);
	},
	init:function(){
		// 电子天平
		this.body = new Button(this, 10, TAG_BALANCE, "#balance.png", this.callback,this);
		//螺帽
		var nut=new Button(this.body,10,TAG_NUT,"#nut.png",this.callback,this);
		nut.setPosition(this.body.width*0.93,this.body.height*0.55);
		//仪表盘
		var instructions=new Button(this.body,10,TAG_INSTRUCTIONS,"#instructions.png",this.callback,this);
		instructions.setPosition(this.body.width*0.5,this.body.height*0.88);
		//游码
		var rider=new Button(this.body,10,TAG_RIDER,"#rider2.png",this.callback,this);
		rider.setPosition(this.body.width*0.22,this.body.height*0.47);
		rider.setScale(0.8);
		//仪表
		var disk=new Button(this.body,10,TAG_DISK,"#disk.png",this.callback,this);
		disk.setPosition(this.body.width*0.5,this.body.height*1.2);
		disk.setOpacity(0);
		disk.setCascadeOpacityEnabled(true);
		//指针
		var point=new Button(disk,10,TAG_POINT,"#point.png",this.callback,this);
		point.setAnchorPoint(cc.p(0.5,0.2));
		point.setPosition(disk.width*0.5,disk.height*0.2);
		point.setRotation(-10);
		//特写游标
		var rider1=new Button(this,10,TAG_RIDER1,"#rider1.png",this.callback,this);
		rider1.setPosition(0,-this.body.height*0.6);
		rider1.setOpacity(0);
		rider1.setCascadeOpacityEnabled(true);
		//特写游码
		var rider2=new Button(rider1,10,TAG_RIDER2,"#rider2.png",this.callback,this);
		rider2.setPosition(rider1.width*0.1,rider1.height*0.5);
	},
	show:function(){
		var disk=this.body.getChildByTag(TAG_DISK);
		var point=disk.getChildByTag(TAG_POINT);
		var fadein=cc.fadeIn(1);
		var fadeout=cc.fadeOut(1);
		disk.runAction(cc.sequence(fadein,cc.delayTime(1),fadeout));
	},
	show1:function(){
		var disk=this.body.getChildByTag(TAG_DISK);
		var point=disk.getChildByTag(TAG_POINT);
		var rotate=cc.rotateBy(0.2,-10);
		var rotate1=cc.rotateBy(0.2,10);
		disk.setOpacity(255);
		point.runAction(cc.sequence(cc.callFunc(function(){
			point.setRotation(0);
		}, this),cc.sequence(rotate,rotate.reverse(),rotate1,rotate1.reverse()).repeat(3),cc.callFunc(function() {
			disk.runAction(cc.fadeOut(1));
		},this)));
	},
	show2:function(){
		var rider1=this.getChildByTag(TAG_RIDER1);
		rider1.setOpacity(255);
		var rider2=rider1.getChildByTag(TAG_RIDER2);
		var move=cc.moveBy(1,cc.p(10,0));
		rider2.runAction(cc.sequence(move,cc.delayTime(2),cc.callFunc(function() {
			rider1.runAction(cc.fadeOut(1));
		},this)));
	},
	show3:function(){
		var disk=this.body.getChildByTag(TAG_DISK);
		var point=disk.getChildByTag(TAG_POINT);
		point.setRotation(0);
		var fadein=cc.fadeIn(1);
		var fadeout=cc.fadeOut(1);
		disk.runAction(cc.sequence(fadein,cc.callFunc(function() {
			point.runAction(cc.rotateBy(1,15));
		},this),cc.delayTime(1),fadeout));
	},
	show4:function(){
		var disk=this.body.getChildByTag(TAG_DISK);
		var point=disk.getChildByTag(TAG_POINT);
		point.setRotation(0);
		var fadein=cc.fadeIn(1);
		var fadeout=cc.fadeOut(1);
		disk.runAction(cc.sequence(fadein,cc.delayTime(1),fadeout));
	},
	callback:function(p){
		var func=cc.callFunc(function(){
			gg.flow.next();
		},this);
		switch(p.getTag()){
		case TAG_NUT:
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.show1();
			}, this),cc.delayTime(4),func));
			break;
		case TAG_INSTRUCTIONS:
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.show();
			},this),cc.delayTime(3),func));
			break;
		case TAG_RIDER:
			var move=cc.moveBy(1,cc.p(20,0));
			p.runAction(cc.sequence(cc.spawn(move,cc.callFunc(function() {
				this.show2();
				this.show3();
			},this)),func));
			break;
		default:
			break;
		}
	}
});