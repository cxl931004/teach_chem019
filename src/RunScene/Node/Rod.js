Rod = Widget.extend({
	ctor:function(){
		this._super();
		this.init();
		this.stir();
		this.setTag(TAG_ROD_ALL);
	},
	init:function(){
		this.body = new Button(this, 10, TAG_ROD, "#rod.png", this.callback,this);
		this.body.setAnchorPoint(cc.p(0.5, 0.85));
	},
	stir:function(){
//		var ber=cc.bezierBy(1,[cc.p(-15,35),cc.p(-190,268),cc.p(-190,15)]);
		var rotate=cc.rotateBy(0.5,5);
		var rotate1=cc.rotateBy(0.5,-5);
		this.body.runAction(cc.sequence(cc.spawn(cc.sequence(rotate,rotate.reverse(),rotate1,rotate1.reverse(),
		rotate,rotate.reverse(),rotate1,rotate1.reverse()),cc.callFunc(function() {
		var flask=ll.run.getChildByTag(TAG_FLASK_ALL);
		flask.showFlask();
		},this)),cc.callFunc(function() {
		this.body.setVisible(false);
		},this)));
	}
});