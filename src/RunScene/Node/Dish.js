Dish = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_DISH_ALL);
	},
	init:function(){
		//玻璃皿
		this.body = new Button(this, 10, TAG_DISH, "#dish.png", this.callback,this);
		this.body.setScale(0.8);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		switch (p.getTag()) {
		case TAG_DISH:
			p.runAction(cc.sequence(cc.callFunc(function() {
				p.setVisible(false);
			},this),func));
			break;
		default:
			break;
		}
	}
});