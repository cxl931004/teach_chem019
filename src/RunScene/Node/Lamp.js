Lamp = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.initAction();
		this.setTag(TAG_LAMP_ALL);
	},
	init:function(){
		//酒精灯
		this.body = new Button(this, 10, TAG_LAMP, "#lamp/lp1.png", this.callback,this);
		
		var lamp1=new Button(this,10,TAG_LAMP1,"#lamp/lp2.png",this.callback,this);
		lamp1.setPosition(cc.p(0, this.body.height*0.45));
		
		var fire=new Button(this.body,10,TAG_FIRE1,"#fire/huo1.png",this.callback,this);
		fire.setPosition(cc.p(this.body.width*0.5,this.body.height*1.2));
		fire.setVisible(false);
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<4;i++){
			var str="fire/huo"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.05);
		this.runningAction=cc.repeatForever(cc.animate(animation));
		this.runningAction.retain();
	},
	show:function(){
		var fire=this.body.getChildByTag(TAG_FIRE1);
		fire.setVisible(true);
		fire.runAction(this.runningAction);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		var action=gg.flow.flow.action;
		var ber=cc.bezierBy(1, [cc.p(10, 15),cc.p(25, 55),cc.p(50,-35)]);
		var ber1=cc.bezierBy(1, [cc.p(-50, 15),cc.p(-100,25),cc.p(-260, -15)]);
		var ber3=cc.bezierBy(1, [cc.p(10, 20),cc.p(15, 35),cc.p(0, 0)]);
		var fire=this.body.getChildByTag(TAG_FIRE1);
		switch (p.getTag()) {
		case TAG_LAMP1:
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(ber,func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(cc.spawn(ber.reverse(),cc.sequence(cc.delayTime(0.5),cc.callFunc(function() {
					fire.setVisible(false);
				},this))),cc.delayTime(0.2),ber3,cc.delayTime(1),cc.callFunc(function() {
					this.body.setVisible(false);
					p.setVisible(false);
				},this),cc.callFunc(function() {
					ll.run.clock.setSpriteFrame("tool/clock2.png");
					ll.run.clock.doing();
				},this),cc.delayTime(4),cc.callFunc(function() {
					ll.run.clock.stop();
				},this),func));
			}
			break;
		case TAG_LAMP:
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(ber1,cc.callFunc(function() {
					ll.run.clock.setSpriteFrame("tool/clock1.png");
					ll.run.clock.doing();
				},this),cc.delayTime(4),cc.callFunc(function() {
					ll.run.clock.stop();
				},this),func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(ber1.reverse(),func));
			}
			break;
		default:
			break;
		}
	}
});