Paper = Widget.extend({
	runningAction:null,
	ctor:function(parent,n){
		this._super();
		this.init();
		this.setTag(TAG_PAPER_ALL);
	},
	init:function(){
		// 称量纸
		this.body = new Button(this, 10, TAG_PAPER, "#paper.png", this.callback,this);
		
		var powder=new Button(this.body,10,TAG_POWDER,"#powder.png",this.callback,this);
		powder.setPosition(cc.p(this.body.width*0.5,this.body.height*0.5));
		powder.setOpacity(0);
		powder.setScale(0.3);
	},
	showPowder:function(){
		var powder=this.body.getChildByTag(TAG_POWDER);
		var fadein=cc.fadeIn(1);
		var scale=cc.scaleTo(1,1);
		powder.runAction(cc.sequence(fadein,scale));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		var powder=this.body.getChildByTag(TAG_POWDER);
		var flask=ll.run.getChildByTag(TAG_FLASK_ALL);
		var balance=ll.run.getChildByTag(TAG_BALANCE_ALL);
		var paper=ll.run.getChildByTag(TAG_PAPER_ALL);
		var paper1=ll.run.getChildByTag(TAG_PAPER1);
		switch (p.getTag()) {
		case TAG_PAPER:
			var ber=cc.bezierBy(1,[cc.p(45, 10),cc.p(150, 30),cc.p(288, 50)]);
			var skew=cc.skewBy(1, -30, -30);
			p.runAction(cc.sequence(ber,cc.spawn(skew,cc.callFunc(function() {
				powder.runAction(cc.spawn(cc.fadeOut(2),cc.callFunc(function() {
					flask.showPowder1();
					flask.showFlow();
				}, this)));
			},this)),cc.delayTime(1),skew.reverse(),ber.reverse(),cc.callFunc(function() {
				balance.setVisible(false);
				paper.setVisible(false);
				paper1.setVisible(false);
			}, this),func));
			break;
		default:
			break;
		}
	}
});