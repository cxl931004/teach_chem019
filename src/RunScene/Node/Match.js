Match = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.ignition();
		this.setTag(TAG_MATCH_ALL);
	},
	init:function(){
		//火柴盒子
		this.body = new Button(this, 10, TAG_MATCH, "#lamp/match_box.png", this.callback,this);
		this.body.setScale(0.8);
		
		var match=new Button(this.body,10,TAG_MATCH1,"#lamp/match.png",this.callback,this);
		match.setPosition(cc.p(this.body.width*0.98,this.body.height));
		match.setOpacity(0);
		
		var fire=new Button(match,10,TAG_FIRE,"#lamp/fire.png",this.callback,this);
		fire.setPosition(cc.p(0,match.height*0.95));
		fire.setOpacity(0);
	},
	ignition:function(){
		var func=cc.callFunc(function() {
			gg.flow.next();
			},this);
		var move=cc.moveBy(1,cc.p(this.body.width*0.5,-this.body.height*0.4));
		var fadein=cc.fadeIn(0.5);
		var match=this.body.getChildByTag(TAG_MATCH1);
		var fire=match.getChildByTag(TAG_FIRE);
		var lamp=ll.run.getChildByTag(TAG_LAMP_ALL);
		var ber=cc.bezierBy(1, [cc.p(-25, 10),cc.p(-230,20),cc.p(-380,38)]);
		match.runAction(cc.sequence(fadein,move,cc.callFunc(function() {
			fire.runAction(fadein);
			},this),ber,cc.delayTime(1),cc.callFunc(function() {
			lamp.show();
			match.setVisible(false);
			this.body.setVisible(false);
			},this),func));
	}
//	callback:function(p){
//		var func=cc.callFunc(function() {
//		gg.flow.next();
//		},this);
//		var move=cc.moveBy(1,cc.p(this.body.width*0.5,-this.body.height*0.4));
//		var fadein=cc.fadeIn(0.5);
//		var match=this.body.getChildByTag(TAG_MATCH1);
//		var fire=match.getChildByTag(TAG_FIRE);
//		var lamp=ll.run.getChildByTag(TAG_LAMP_ALL);
//		var ber=cc.bezierBy(1, [cc.p(-25, 10),cc.p(-230,20),cc.p(-380,38)]);
//		switch (p.getTag()) {
//		case TAG_MATCH:
//			match.runAction(cc.sequence(fadein,move,cc.callFunc(function() {
//				fire.runAction(fadein);
//			},this),ber,cc.delayTime(1),cc.callFunc(function() {
//				lamp.show();
//				match.setVisible(false);
//				this.body.setVisible(false);
//			},this),func));
//			break;
//		default:
//			break;
//		}
//	}
});