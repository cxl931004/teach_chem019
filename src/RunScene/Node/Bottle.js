//棕色瓶
Bottle = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_BOTTLE_ALL);
	},
	init:function(){

		this.body = new Button(this, 10, TAG_BOTTLE, "#bottle/bottle1.png", this.callback,this);
		
		var lid=new Button(this,9,TAG_LID,"#bottle/lid1.png",this.callback,this);
		lid.setPosition(this.body.width*0.05,this.body.height*0.3);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_LID:
			var ber=cc.bezierBy(1, [cc.p(10, 85),cc.p(55, -30),cc.p(65, -65)]);
			var rotate=cc.rotateBy(1,180);
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(cc.spawn(ber.reverse(),rotate.reverse()),cc.callFunc(function() {
					this.body.setVisible(false);
					p.setVisible(false);
				},this),func));
			}else{
				p.runAction(cc.sequence(cc.spawn(ber,rotate),func));
			}
			break;
		default:
			break;
		}
	}
});