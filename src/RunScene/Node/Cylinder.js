/**
 * 量筒
 */
Cylinder = Widget.extend({
	runningAction:null,
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_CYLINDER_ALL);
	},
	init:function(){
		this.body = new Button(this, 10, TAG_CYLINDER, "#cylinder.png", this.callback,this);
		
		var flow=new Button(this.body,10,TAG_FILM,"#film.png",this.callback,this);
		flow.setPosition(cc.p(this.body.width*0.5,this.body.height*0.08));
		flow.setScale(0.3);
		flow.setOpacity(0);
		
		var show=new Button(this.body,10,TAG_SHOW,"#show.png",this.callback,this);
		show.setPosition(cc.p(this.body.width*1.85,this.body.height*0.8));
		show.setOpacity(0);
	},
	show:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var show=this.body.getChildByTag(TAG_SHOW);
		show.runAction(cc.sequence(fadein,cc.delayTime(1),fadeout));
	},
	
	showFlow:function(){
		var move=cc.moveBy(1,cc.p(0, this.body.height*0.65));
		var fadein=cc.fadeIn(0.5);
		var flow=this.body.getChildByTag(TAG_FILM);
		flow.runAction(cc.sequence(fadein,move));
	},
	back:function(){
		var flow=this.body.getChildByTag(TAG_FILM);
		var rotate=cc.rotateBy(1,-85);
		var scale=cc.scaleBy(1,2.8);
		var move=cc.moveBy(1,cc.p(0,-this.body.height*0.58));
		var fadeout=cc.fadeOut(1);
		flow.runAction(cc.sequence(cc.spawn(rotate.reverse(),scale),move,fadeout));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		var flow=this.body.getChildByTag(TAG_FILM);
		var ber=cc.bezierBy(1, [cc.p(-15,20),cc.p(-35,45),cc.p(-100,65)]);
		var rotate=cc.rotateBy(1,-85);
		var scale=cc.scaleBy(1,2.8);
		var move=cc.moveBy(1,cc.p(0,-this.body.height*0.58));
		var fadeout=cc.fadeOut(1);
		switch (p.getTag()){
		case TAG_CYLINDER:
			p.runAction(cc.sequence(ber,cc.spawn(rotate,cc.callFunc(function() {
				this.back();
			},this),cc.callFunc(function(){
				var flask=ll.run.getChildByTag(TAG_FLASK_ALL);
				flask.showFilm();
			},this)),cc.delayTime(2),cc.spawn(rotate.reverse()
			),cc.callFunc(function() {
				p.setVisible(false);
			},this),func));
			break;
		default:
			break;
		}
	}
});