Drop = Widget.extend({
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_DROP_ALL);
	},
	init:function(){
		this.body = new Button(this, 10, TAG_DROP, "#drop.png", this.callback,this);
		
		var right1=new Button(this,10,TAG_RIGHT31,"#Drop/right1.png",this.callback,this);
		right1.setPosition(this.body.width*2.5,this.body.height*0.04);
		right1.setVisible(false)
		
		var right2=new Button(right1,10,TAG_RIGHT32,"#Drop/right2.png",this.callback,this);
		right2.setPosition(cc.p(right1.width*0.5, right1.height*0.5));
		
		var right3=new Button(right1,10,TAG_RIGHT33,"#Drop/right3.png",this.callback,this);
		right3.setPosition(cc.p(right1.width*0.5, right1.height*0.5));
		
	},
	showWater:function(){
		var right1=this.getChildByTag(TAG_RIGHT31);
		var water=new Button(right1,10,TAG_WATER,"#water.png",this.callback,this);
		water.setPosition(right1.width*0.27,right1.height*0.05);
		water.runAction(cc.sequence(cc.moveBy(1,cc.p(0,-20)),cc.callFunc(function() {
			if(water.getPositionY()<right1.height*0.05-10){
				water.removeFromParent();
				this.showWater();
			}
		},this)));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		var right1=this.getChildByTag(TAG_RIGHT31);
		var flask=ll.run.getChildByTag(TAG_FLASK_ALL);
		switch (p.getTag()) {
		case TAG_DROP:
			p.setVisible(false);
			right1.setVisible(true);
			var ber=cc.bezierBy(1, [cc.p(-10,35),cc.p(-50, 130),cc.p(-120,50)]);
			var ber1=cc.bezierBy(1,[cc.p(-20, 35),cc.p(-50, 80),cc.p(-260,180)]);
			right1.runAction(cc.sequence(ber,cc.delayTime(2),ber1,cc.spawn(cc.callFunc(function() {
				this.showWater();
			},this),cc.callFunc(function() {
				flask.show1();
			},this)),cc.delayTime(2),cc.callFunc(function() {
				right1.setVisible(false);
			},this),func));
			break;
		default:
			break;
		}
	}
});