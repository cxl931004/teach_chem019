var AboutBackgroundLayer = cc.Layer.extend({
    ctor:function () {
        this._super();
        this.loadBg();
        this.loadAnim();
        return true;
    },
    loadBg : function(){
    	var node = new cc.Sprite("#about_bg.png");
        this.addChild(node);
        node.setPosition(gg.c_p);
    },
    loadAnim:function(){
    	var logo = new cc.Sprite("#about_logo.png");
    	logo.setPosition(320, 384);
    	this.addChild(logo);
    	var plane = new cc.Sprite("#about_plane.png");
    	plane.setPosition(logo.width * 0.5, logo.height * 0.5);
    	logo.addChild(plane);
    	var repeat = cc.repeatForever(cc.rotateBy(3, 360));
    	plane.runAction(repeat);
    }
});