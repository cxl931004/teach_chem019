/**
 * 文字自带边框的实体
 */
ShowTip = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this);
		this.setCascadeOpacityEnabled(true);
	},
	init:function(name, delay){
		this.draw = new cc.DrawNode();
		this.addChild(this.draw);
		var content = new cc.LabelTTF(name, gg.fontName, gg.fontSize);
		content.setColor(cc.color(0, 0, 0));
		this.addChild(content);
		var rect = content.getBoundingBox();
		var mr = 10;
		this.draw.drawRect(cc.p(rect.x - mr,rect.y - mr),
				cc.p(rect.x + rect.width + mr,rect.y + rect.height + mr),
				cc.color(255,255,255,150), 2, cc.color(0,25,200,150));
		
		if(delay == null){
			delay = 2;
		}
		this.scheduleOnce(this.kill, delay);
	},
	kill:function(){
		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
			this.removeFromParent(true);	
		}, this))
		this.runAction(seq);
	}
})