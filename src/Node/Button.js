var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
		this.setEnable(false);
	},
	preCall:function(){
		// 隐藏箭头
		ll.tip.arr.out();
		this.setEnable(false);
		// 操作成功
		gg.score += 10;
		if(!gg.errFlag){
			gg.oneSure ++;
		}
		gg.errFlag = false;
		_.clever();
	},
	exeUnEnable:function(){
		// 操作失败
		gg.score -= 3;
		if(gg.score<0){
			gg.score=0;
		}
		gg.errFlag = true;
		gg.errorStep ++;
		_.error();
	}
})